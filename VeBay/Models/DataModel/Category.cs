﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VeBay.Models.DataModel
{
    public class Category
    {
        [Key]
        public Guid CategoryID { get; set; }
        [Required]
        public string CategoryName { get; set; }
        public string CategoryAvatar { get; set; }
        public ICollection<ChildCategory> ChildCategories { get; set; }
    }
}