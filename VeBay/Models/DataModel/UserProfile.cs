﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace VeBay.Models.DataModel
{
    public class UserProfile
    {
        [Key]
        public Guid UserProfileID { get; set; }
        [Required]
        public string UserID { get; set; }
        public ApplicationUser User { get; set; }

        public string Phone { get; set; }
        public int WardID { get; set; }
        public string UserAvatar { get; set; }
        public DateTime DateOfBirth { get; set; }
        public int Gender { get; set; }
        [MaxLength(200)]
        [MinLength(1)]
        public string FullName { get; set; }

    }
}