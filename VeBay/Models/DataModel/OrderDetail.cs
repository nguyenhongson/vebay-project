﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace VeBay.Models.DataModel
{
    public class OrderDetail
    {
        [Key]
        public Guid OrderDetailID { get; set; }
        [Required]
        public Guid OrderID { get; set; }
        public Order Order { get; set; }
        [Required]
        public Guid ProductID { get; set; }
        public float Price { get; set; }
        
        public int Quantity { get; set; }
        public int Status { get; set; }
        public DateTime TimeUpdate { get; set; }

    }
}