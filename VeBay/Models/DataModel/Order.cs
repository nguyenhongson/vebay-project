﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace VeBay.Models.DataModel
{
    public class Order
    {
        [Key]
        public Guid OrderID { get; set; }
        public float TotalMoney { get; set; }
        public DateTime CreateAt { get; set; }
        [Required]
        public  string UserId  { get; set; }
        public ApplicationUser User { get; set; }

        public int Status { get; set; }
        public DateTime TimeUpdate { get; set; }

        public ICollection<OrderDetail> OrderDetails { get; set; }
    }
}