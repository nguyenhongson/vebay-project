﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace VeBay.Models.DataModel
{
    public class ProductColor
    {
        [Key]
        public Guid ProductColorID { get; set; }
        [Required]
        public Guid ProductID { get; set; }
        public Product Product { get; set; }
        [MaxLength(200)]
        public string ColorName { get; set; }
    }
}