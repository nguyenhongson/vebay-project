﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace VeBay.Models.DataModel
{
    public class Product
    {
        [Key]
        public Guid ProductID { get; set; }
        public string ProductName { get; set; }
        [Required]
        public Guid ShopID { get; set; }
        public Shop Shop { get; set; }
        public int Quantity { get; set; }
        public string ProductAvatar { get; set; }
        [MinLength(1)]
        public string Description { get; set; }
        public int Status { get; set; }
        [Required]
        public int MinOrder { get; set; }
        public int Price { get; set; }

        public Guid ChildCategoryID { get; set; }
        public ChildCategory ChildCategory { get; set; }

        public ICollection<ProductPhoto> ProductPhotos { get; set; }
        public ICollection<ProductColor> ProductColors { get; set; }
        public ICollection<ProductPromotion> ProductPromotions { get; set; }
    }
}