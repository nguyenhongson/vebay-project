﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace VeBay.Models.DataModel
{
    public class Shop
    {
        [Key]
        public Guid ShopID { get; set; }
        [Required]
        public Guid UserID { get; set; }
        public ApplicationUser User { get; set; }

        [MaxLength(300)]
        public string ShopName { get; set; }
        public int WardID { get; set; }
        public Ward Ward { get; set; }
        public int Status { get; set; }

        public ICollection<Commission> Commissions { get; set; }
        public ICollection<Product> Products { get; set; }
    }
}