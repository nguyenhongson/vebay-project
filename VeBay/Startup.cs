﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(VeBay.Startup))]
namespace VeBay
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
