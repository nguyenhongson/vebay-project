namespace VeBay.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addDbVeBay : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        CategoryID = c.Guid(nullable: false),
                        CategoryName = c.String(nullable: false),
                        CategoryAvatar = c.String(),
                    })
                .PrimaryKey(t => t.CategoryID);
            
            CreateTable(
                "dbo.ChildCategories",
                c => new
                    {
                        ChildCategoryID = c.Guid(nullable: false),
                        Category = c.Guid(nullable: false),
                        CategoryName = c.String(nullable: false),
                        CategoryAvatar = c.String(),
                    })
                .PrimaryKey(t => t.ChildCategoryID);
            
            CreateTable(
                "dbo.Commissions",
                c => new
                    {
                        CommissionID = c.Guid(nullable: false),
                        ShopID = c.Guid(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        PayDate = c.DateTime(nullable: false),
                        TotalMoney = c.Single(nullable: false),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CommissionID);
            
            CreateTable(
                "dbo.Districts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        ProvinceId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        OrderID = c.Guid(nullable: false),
                        TotalMoney = c.Single(nullable: false),
                        CreateAt = c.DateTime(nullable: false),
                        UserId = c.Guid(nullable: false),
                        Status = c.Int(nullable: false),
                        TimeUpdate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.OrderID);
            
            CreateTable(
                "dbo.OrderDetails",
                c => new
                    {
                        OrderDetailID = c.Guid(nullable: false),
                        OrderID = c.Guid(nullable: false),
                        ProductID = c.Guid(nullable: false),
                        Price = c.Single(nullable: false),
                        Quantity = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        TimeUpdate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.OrderDetailID);
            
            CreateTable(
                "dbo.Payments",
                c => new
                    {
                        PaymentID = c.Guid(nullable: false),
                        PaymentName = c.String(),
                        PaymentGuide = c.String(),
                        PaymentAvatar = c.String(),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PaymentID);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        ProductID = c.Guid(nullable: false),
                        ProductName = c.String(),
                        ShopID = c.Guid(nullable: false),
                        Quantity = c.Int(nullable: false),
                        ProductAvatar = c.String(),
                        Description = c.String(),
                        Status = c.Int(nullable: false),
                        MinOrder = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ProductID);
            
            CreateTable(
                "dbo.ProductColors",
                c => new
                    {
                        ProductColorID = c.Guid(nullable: false),
                        ProductID = c.Guid(nullable: false),
                        ColorName = c.String(maxLength: 200),
                    })
                .PrimaryKey(t => t.ProductColorID);
            
            CreateTable(
                "dbo.ProductPhotoes",
                c => new
                    {
                        PoductPhotoID = c.Guid(nullable: false),
                        ProductID = c.Guid(nullable: false),
                        PhotoPath = c.String(),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PoductPhotoID);
            
            CreateTable(
                "dbo.ProductPromotions",
                c => new
                    {
                        ProductPromotionID = c.Guid(nullable: false),
                        ProductID = c.Guid(nullable: false),
                        ContentPromotion = c.String(),
                    })
                .PrimaryKey(t => t.ProductPromotionID);
            
            CreateTable(
                "dbo.Provinces",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Shops",
                c => new
                    {
                        ShopID = c.Guid(nullable: false),
                        UserID = c.Guid(nullable: false),
                        ShopName = c.String(maxLength: 300),
                        WardID = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ShopID);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        TagID = c.Guid(nullable: false),
                        CategoryID = c.Guid(nullable: false),
                        ChildCategoryID = c.Guid(nullable: false),
                        TagName = c.String(),
                    })
                .PrimaryKey(t => t.TagID);
            
            CreateTable(
                "dbo.UserProfiles",
                c => new
                    {
                        UserProfileID = c.Guid(nullable: false),
                        UserID = c.Guid(nullable: false),
                        Phone = c.String(),
                        WardID = c.Int(nullable: false),
                        UserAvatar = c.String(),
                        DateOfBirth = c.DateTime(nullable: false),
                        Gender = c.Int(nullable: false),
                        FullName = c.String(maxLength: 200),
                    })
                .PrimaryKey(t => t.UserProfileID);
            
            CreateTable(
                "dbo.Wards",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        DistrictId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Wards");
            DropTable("dbo.UserProfiles");
            DropTable("dbo.Tags");
            DropTable("dbo.Shops");
            DropTable("dbo.Provinces");
            DropTable("dbo.ProductPromotions");
            DropTable("dbo.ProductPhotoes");
            DropTable("dbo.ProductColors");
            DropTable("dbo.Products");
            DropTable("dbo.Payments");
            DropTable("dbo.OrderDetails");
            DropTable("dbo.Orders");
            DropTable("dbo.Districts");
            DropTable("dbo.Commissions");
            DropTable("dbo.ChildCategories");
            DropTable("dbo.Categories");
        }
    }
}
