namespace VeBay.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addFOREIGNKEY : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ChildCategories", "CategoryID", c => c.Guid(nullable: false));
            AddColumn("dbo.Products", "Price", c => c.Single(nullable: false));
            AddColumn("dbo.Shops", "User_Id", c => c.String(maxLength: 128));
            AlterColumn("dbo.Orders", "UserId", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.ChildCategories", "CategoryID");
            CreateIndex("dbo.Commissions", "ShopID");
            CreateIndex("dbo.Shops", "WardID");
            CreateIndex("dbo.Shops", "User_Id");
            CreateIndex("dbo.Orders", "UserId");
            CreateIndex("dbo.Wards", "DistrictId");
            CreateIndex("dbo.Districts", "ProvinceId");
            CreateIndex("dbo.OrderDetails", "OrderID");
            CreateIndex("dbo.Products", "ShopID");
            CreateIndex("dbo.ProductColors", "ProductID");
            CreateIndex("dbo.ProductPhotoes", "ProductID");
            CreateIndex("dbo.ProductPromotions", "ProductID");
            AddForeignKey("dbo.ChildCategories", "CategoryID", "dbo.Categories", "CategoryID", cascadeDelete: true);
            AddForeignKey("dbo.Orders", "UserId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Shops", "User_Id", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.Districts", "ProvinceId", "dbo.Provinces", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Wards", "DistrictId", "dbo.Districts", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Shops", "WardID", "dbo.Wards", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Commissions", "ShopID", "dbo.Shops", "ShopID", cascadeDelete: true);
            AddForeignKey("dbo.OrderDetails", "OrderID", "dbo.Orders", "OrderID", cascadeDelete: true);
            AddForeignKey("dbo.Products", "ShopID", "dbo.Shops", "ShopID", cascadeDelete: true);
            AddForeignKey("dbo.ProductColors", "ProductID", "dbo.Products", "ProductID", cascadeDelete: true);
            AddForeignKey("dbo.ProductPhotoes", "ProductID", "dbo.Products", "ProductID", cascadeDelete: true);
            AddForeignKey("dbo.ProductPromotions", "ProductID", "dbo.Products", "ProductID", cascadeDelete: true);
            DropColumn("dbo.ChildCategories", "Category");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ChildCategories", "Category", c => c.Guid(nullable: false));
            DropForeignKey("dbo.ProductPromotions", "ProductID", "dbo.Products");
            DropForeignKey("dbo.ProductPhotoes", "ProductID", "dbo.Products");
            DropForeignKey("dbo.ProductColors", "ProductID", "dbo.Products");
            DropForeignKey("dbo.Products", "ShopID", "dbo.Shops");
            DropForeignKey("dbo.OrderDetails", "OrderID", "dbo.Orders");
            DropForeignKey("dbo.Commissions", "ShopID", "dbo.Shops");
            DropForeignKey("dbo.Shops", "WardID", "dbo.Wards");
            DropForeignKey("dbo.Wards", "DistrictId", "dbo.Districts");
            DropForeignKey("dbo.Districts", "ProvinceId", "dbo.Provinces");
            DropForeignKey("dbo.Shops", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Orders", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.ChildCategories", "CategoryID", "dbo.Categories");
            DropIndex("dbo.ProductPromotions", new[] { "ProductID" });
            DropIndex("dbo.ProductPhotoes", new[] { "ProductID" });
            DropIndex("dbo.ProductColors", new[] { "ProductID" });
            DropIndex("dbo.Products", new[] { "ShopID" });
            DropIndex("dbo.OrderDetails", new[] { "OrderID" });
            DropIndex("dbo.Districts", new[] { "ProvinceId" });
            DropIndex("dbo.Wards", new[] { "DistrictId" });
            DropIndex("dbo.Orders", new[] { "UserId" });
            DropIndex("dbo.Shops", new[] { "User_Id" });
            DropIndex("dbo.Shops", new[] { "WardID" });
            DropIndex("dbo.Commissions", new[] { "ShopID" });
            DropIndex("dbo.ChildCategories", new[] { "CategoryID" });
            AlterColumn("dbo.Orders", "UserId", c => c.Guid(nullable: false));
            DropColumn("dbo.Shops", "User_Id");
            DropColumn("dbo.Products", "Price");
            DropColumn("dbo.ChildCategories", "CategoryID");
        }
    }
}
