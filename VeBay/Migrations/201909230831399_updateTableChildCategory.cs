namespace VeBay.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateTableChildCategory : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ChildCategories", "ChildCategoryName", c => c.String(nullable: false));
            AddColumn("dbo.ChildCategories", "ChildCategoryAvatar", c => c.String());
            DropColumn("dbo.ChildCategories", "CategoryName");
            DropColumn("dbo.ChildCategories", "CategoryAvatar");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ChildCategories", "CategoryAvatar", c => c.String());
            AddColumn("dbo.ChildCategories", "CategoryName", c => c.String(nullable: false));
            DropColumn("dbo.ChildCategories", "ChildCategoryAvatar");
            DropColumn("dbo.ChildCategories", "ChildCategoryName");
        }
    }
}
