namespace VeBay.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addManyRecordToDatabase : DbMigration
    {
        public override void Up()
        {
            Sql(@"
                INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'0d8756bb-6cd6-4b52-8cae-bfc6c98a983b', N'admin@VeBay.com', 0, N'AL65hdG5qfJBFIyJzO+TUHmCDjWezHytC4wCTu5QpzwCosxR+2eFMDunZEkaUZad2A==', N'23a4a3b5-9563-4bb3-bc80-52a195e508f0', NULL, 0, 0, NULL, 1, 0, N'admin@VeBay.com')
                INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'884eb30e-ded9-4965-9d48-3e294eec5e10', N'Shop@VeBay.com', 0, N'AHPU9FI5wHxAgYrkiPg4FNYJnA/RKd0Khw3FVYxiwstSIwqlLBpESSh0XixkunmXzQ==', N'bf1bf95b-892c-498f-aa01-f56f77dd19a7', NULL, 0, 0, NULL, 1, 0, N'Shop@VeBay.com')
                INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'fd77aebe-e872-46ea-95a9-3d882fef0868', N'User@VeBay.com', 0, N'AFG72czBF7vEC1deLcxTCqHGK1p6L24cxus4VSmJzQqeB+uFNFWA8WEKWwI12PSavw==', N'f681c060-f8f9-4d57-aa7a-e5f14ebe7f65', NULL, 0, 0, NULL, 1, 0, N'User@VeBay.com')
                INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'431cbbe0-5340-47e6-bb0b-43025f6b7354', N'Admin')
                INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'c964992b-d1e6-4c30-8776-b9710bb5f9fe', N'Shop')
                INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'0d8756bb-6cd6-4b52-8cae-bfc6c98a983b', N'431cbbe0-5340-47e6-bb0b-43025f6b7354')
                INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'884eb30e-ded9-4965-9d48-3e294eec5e10', N'c964992b-d1e6-4c30-8776-b9710bb5f9fe')
                INSERT INTO [dbo].[Categories] ([CategoryId], [CategoryName], [CategoryAvatar]) VALUES (N'eacbd8d7-e3ff-45ca-b351-19cc4b75aead', N'Mobile Phone', NULL)
                INSERT INTO [dbo].[ChildCategories] ([ChildCategoryId], [CategoryId], [ChildCategoryName], [ChildCategoryAvatar]) VALUES (N'da3e8bbf-6f20-4733-90b7-275670413c10', N'eacbd8d7-e3ff-45ca-b351-19cc4b75aead', N'Iphone', NULL)
                INSERT INTO [dbo].[Products] ([ProductId], [ProductName], [ShopId], [ProductAvatar], [Price], [Description], [Status], [MinOrder], [Quantity], [ChildCategoryId]) VALUES (N'6eebf54e-ddba-416a-a46d-1da109d7d0df', N'Iphone Xs Max', N'8acc0f6d-fb15-4047-9997-23853a89e18c', NULL, 22000000, N'BrandNew', 1, 2, 2, N'da3e8bbf-6f20-4733-90b7-275670413c10')
                INSERT INTO [dbo].[Products] ([ProductId], [ProductName], [ShopId], [ProductAvatar], [Price], [Description], [Status], [MinOrder], [Quantity], [ChildCategoryId]) VALUES (N'b2beb0fd-3279-48c8-a253-dd40b2dbd5f3', N'Iphone 8 Plus', N'55804bd3-c065-4b6c-8697-f3ebfac4bfaf', NULL, 10000000, N'BrandNew', 1, 1, 3, N'da3e8bbf-6f20-4733-90b7-275670413c10')
                ");

        }

        public override void Down()
        {
        }
    }
}
