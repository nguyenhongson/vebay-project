namespace VeBay.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateForeignkey2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "ChildCategoryID", c => c.Guid(nullable: false));
            AlterColumn("dbo.UserProfiles", "UserID", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.Products", "ChildCategoryID");
            CreateIndex("dbo.UserProfiles", "UserID");
            CreateIndex("dbo.UserProfiles", "WardID");
            AddForeignKey("dbo.Products", "ChildCategoryID", "dbo.ChildCategories", "ChildCategoryID", cascadeDelete: true);
            AddForeignKey("dbo.UserProfiles", "UserID", "dbo.AspNetUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.UserProfiles", "WardID", "dbo.Wards", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserProfiles", "WardID", "dbo.Wards");
            DropForeignKey("dbo.UserProfiles", "UserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.Products", "ChildCategoryID", "dbo.ChildCategories");
            DropIndex("dbo.UserProfiles", new[] { "WardID" });
            DropIndex("dbo.UserProfiles", new[] { "UserID" });
            DropIndex("dbo.Products", new[] { "ChildCategoryID" });
            AlterColumn("dbo.UserProfiles", "UserID", c => c.Guid(nullable: false));
            DropColumn("dbo.Products", "ChildCategoryID");
        }
    }
}
