// <auto-generated />
namespace VeBay.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class addManyRecordToDatabase : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(addManyRecordToDatabase));
        
        string IMigrationMetadata.Id
        {
            get { return "201909231033204_addManyRecordToDatabase"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
