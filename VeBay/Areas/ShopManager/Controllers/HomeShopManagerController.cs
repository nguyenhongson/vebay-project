﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VeBay.Areas.ShopManager.Controllers
{
    public class HomeShopManagerController : Controller
    {
        // GET: ShopManager/HomeShopManager
        public ActionResult Index()
        {
            //check session user if role=shopmanager
            return View();
        }
    }
}